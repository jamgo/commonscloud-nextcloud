set -x

mkdir -p /srv/docker-data/nextcloud/db-data
mkdir -p /srv/docker-data/nextcloud/db-conf
mkdir -p /srv/docker-data/nextcloud/app
mkdir -p /srv/docker-data/nextcloud/data

docker volume create --name nextcloud-db-data \
 --opt type=none \
 --opt device=/srv/docker-data/nextcloud/db-data \
 --opt o=bind
docker volume create --name nextcloud-db-conf \
 --opt type=none \
 --opt device=/srv/docker-data/nextcloud/db-conf \
 --opt o=bind
docker volume create --name nextcloud-app \
 --opt type=none \
 --opt device=/srv/docker-data/nextcloud/app \
 --opt o=bind
docker volume create --name nextcloud-data \
 --opt type=none \
 --opt device=/srv/docker-data/nextcloud/data \
 --opt o=bind

docker-compose up -d db
sleep 10
docker-compose run --user www-data app php index.php
docker-compose run --user www-data -v ${PWD}/configure-nextcloud.sh:/tmp/configure-nextcloud.sh app bash /tmp/configure-nextcloud.sh

docker-compose up --no-start

docker network connect bridge nextcloud
docker network connect nextcloud ldap
