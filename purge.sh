#!/bin/bash

purge() {
	echo "Purging..."
	docker network disconnect nextcloud ldap
	docker-compose down
	docker volume rm nextcloud-db-data nextcloud-db-conf nextcloud-app nextcloud-data
	sudo rm -rf /srv/docker-data/nextcloud
	exit
}

while true
do
	read -r -p "Purge will remove also external volume data. Are you Sure? To only stop container, use stop.sh [yes/no] " input

	case $input in
		[yY][eE][sS])
		purge
		;;

		[nN][oO])
		echo "Bye"
		exit
		;;

		*)
		echo "Pleas enter 'yes' or 'no'"
		;;
	esac
done