#!/bin/bash

set -x

php /var/www/html/occ config:system:set trusted_domains 0 --value="${NEXTCLOUD_HOST}"
php /var/www/html/occ config:system:set lost_password_link --value="https://www.commonscloud.coop/recover-password"
php /var/www/html/occ background:cron

# ... Apps

php /var/www/html/occ app:install contacts
php /var/www/html/occ app:enable contacts
php /var/www/html/occ app:install calendar
php /var/www/html/occ app:enable calendar
php /var/www/html/occ app:install circles
php /var/www/html/occ app:enable circles

# ... SMTP

php /var/www/html/occ config:system:set mail_smtpmode --type string --value="smtp"
php /var/www/html/occ config:system:set mail_smtpauthtype --type string --value="Plain"
php /var/www/html/occ config:system:set mail_smtphost --type string --value="docker-host"
php /var/www/html/occ config:system:set mail_smtpport --type string --value="25"
php /var/www/html/occ config:system:set mail_from_address --type string --value="no-reply"
php /var/www/html/occ config:system:set mail_domain --type string --value="commonscloud.coop"

# ... THEME

php /var/www/html/occ config:app:set theming url --value="${NEXTCLOUD_THEMING_URL}"
php /var/www/html/occ config:app:set theming color --value="${NEXTCLOUD_THEMING_COLOR}"
php /var/www/html/occ config:app:set theming name --value="${NEXTCLOUD_THEMING_NAME}"
php /var/www/html/occ config:app:set theming slogan --value="${NEXTCLOUD_THEMING_SLOGAN}"

# ... LDAP

php /var/www/html/occ app:install user_ldap
php /var/www/html/occ app:enable user_ldap
php /var/www/html/occ ldap:create-empty-config

php /var/www/html/occ ldap:set-config 's01' ldapBase dc=commonscloud,dc=coop
php /var/www/html/occ ldap:set-config 's01' ldapBaseGroups ou=collectives,o=femprocomuns,dc=commonscloud,dc=coop
php /var/www/html/occ ldap:set-config 's01' ldapBaseUsers dc=commonscloud,dc=coop
php /var/www/html/occ ldap:set-config 's01' ldapGroupFilter "(&(|(objectclass=groupOfNames)))"
php /var/www/html/occ ldap:set-config 's01' ldapLoginFilter "(&(&(|(objectclass=inetOrgPerson))(|(memberof=cn=${NEXTCLOUD_LDAP_SERVICE},ou=serveis,o=femprocomuns,dc=commonscloud,dc=coop)))(uid=%uid))"
php /var/www/html/occ ldap:set-config 's01' ldapUserFilter "(&(|(objectclass=inetOrgPerson))(|(memberof=cn=${NEXTCLOUD_LDAP_SERVICE},ou=serveis,o=femprocomuns,dc=commonscloud,dc=coop)))"
#php /var/www/html/occ ldap:set-config 's01' ldapUserFilterGroups "${NEXTCLOUD_LDAP_USER_FILTER_GROUPS}"
php /var/www/html/occ ldap:set-config 's01' ldapHost ldap://ldap
php /var/www/html/occ ldap:set-config 's01' ldapPort 389
php /var/www/html/occ ldap:set-config 's01' ldapAgentName cn=nobody,dc=commonscloud,dc=coop
php /var/www/html/occ ldap:set-config 's01' ldapAgentPassword "${LDAP_PASSWORD}"
php /var/www/html/occ ldap:set-config 's01' ldapQuotaAttribute pager
php /var/www/html/occ ldap:set-config 's01' ldapQuotaDefault "512 MB"

php /var/www/html/occ ldap:set-config 's01' ldapUserFilterObjectclass inetOrgPerson
php /var/www/html/occ ldap:set-config 's01' ldapGroupFilterObjectclass groupOfNames
php /var/www/html/occ ldap:set-config 's01' ldapGroupMemberAssocAttr member
php /var/www/html/occ ldap:set-config 's01' ldapExpertUUIDGroupAttr cn
php /var/www/html/occ ldap:set-config 's01' ldapExpertUUIDUserAttr uid
php /var/www/html/occ ldap:set-config 's01' ldapGidNumber gidNumber
php /var/www/html/occ ldap:set-config 's01' ldapGroupDisplayName cn
php /var/www/html/occ ldap:set-config 's01' ldapEmailAttribute mail

php /var/www/html/occ ldap:set-config 's01' hasMemberOfFilterSupport 1
php /var/www/html/occ ldap:set-config 's01' lastJpegPhotoLookup 0
php /var/www/html/occ ldap:set-config 's01' ldapCacheTTL 600
php /var/www/html/occ ldap:set-config 's01' ldapExperiencedAdmin 0  
php /var/www/html/occ ldap:set-config 's01' ldapGroupFilterMode 1  
php /var/www/html/occ ldap:set-config 's01' ldapLoginFilterEmail 0  
php /var/www/html/occ ldap:set-config 's01' ldapLoginFilterMode 0  
php /var/www/html/occ ldap:set-config 's01' ldapLoginFilterUsername 1  
php /var/www/html/occ ldap:set-config 's01' ldapNestedGroups 0  
php /var/www/html/occ ldap:set-config 's01' ldapPagingSize 500
php /var/www/html/occ ldap:set-config 's01' ldapTLS 0  
php /var/www/html/occ ldap:set-config 's01' ldapUserDisplayName displayname
php /var/www/html/occ ldap:set-config 's01' ldapUserFilterMode 0
php /var/www/html/occ ldap:set-config 's01' ldapUuidGroupAttribute auto
php /var/www/html/occ ldap:set-config 's01' ldapUuidUserAttribute auto
php /var/www/html/occ ldap:set-config 's01' turnOffCertCheck 0
php /var/www/html/occ ldap:set-config 's01' turnOnPasswordChange 0
php /var/www/html/occ ldap:set-config 's01' useMemberOfToDetectMembership 1

php /var/www/html/occ ldap:set-config 's01' ldapConfigurationActive 1

php /var/www/html/occ ldap:create-empty-config

php /var/www/html/occ ldap:set-config 's02' ldapBase dc=commonscloud,dc=coop
php /var/www/html/occ ldap:set-config 's02' ldapBaseGroups ou=collectives,o=femprocomuns,dc=commonscloud,dc=coop
php /var/www/html/occ ldap:set-config 's02' ldapBaseUsers dc=commonscloud,dc=coop
php /var/www/html/occ ldap:set-config 's02' ldapGroupFilter "(&(|(objectclass=groupOfNames)))"
php /var/www/html/occ ldap:set-config 's02' ldapLoginFilter "(&(&(|(objectclass=inetOrgPerson))(|(memberof=cn=${NEXTCLOUD_LDAP_SERVICE},ou=serveis,o=femprocomuns,dc=commonscloud,dc=coop)))(uid=%uid))"
php /var/www/html/occ ldap:set-config 's02' ldapUserFilter "(&(|(objectclass=inetOrgPerson))(|(memberof=cn=${NEXTCLOUD_LDAP_SERVICE},ou=serveis,o=femprocomuns,dc=commonscloud,dc=coop)))"
#php /var/www/html/occ ldap:set-config 's02' ldapUserFilterGroups "${NEXTCLOUD_LDAP_USER_FILTER_GROUPS}"
php /var/www/html/occ ldap:set-config 's02' ldapHost ldaps://ldap1.femprocomuns.cat
php /var/www/html/occ ldap:set-config 's02' ldapPort 636
php /var/www/html/occ ldap:set-config 's02' ldapAgentName cn=nobody,dc=commonscloud,dc=coop
php /var/www/html/occ ldap:set-config 's02' ldapAgentPassword "${LDAP_PASSWORD}"
php /var/www/html/occ ldap:set-config 's02' ldapQuotaAttribute pager
php /var/www/html/occ ldap:set-config 's02' ldapQuotaDefault "512 MB"

php /var/www/html/occ ldap:set-config 's02' ldapUserFilterObjectclass inetOrgPerson
php /var/www/html/occ ldap:set-config 's02' ldapGroupFilterObjectclass groupOfNames
php /var/www/html/occ ldap:set-config 's02' ldapGroupMemberAssocAttr member
php /var/www/html/occ ldap:set-config 's02' ldapExpertUUIDGroupAttr cn
php /var/www/html/occ ldap:set-config 's02' ldapExpertUUIDUserAttr uid
php /var/www/html/occ ldap:set-config 's02' ldapGidNumber gidNumber
php /var/www/html/occ ldap:set-config 's02' ldapGroupDisplayName cn
php /var/www/html/occ ldap:set-config 's02' ldapEmailAttribute mail

php /var/www/html/occ ldap:set-config 's02' hasMemberOfFilterSupport 1
php /var/www/html/occ ldap:set-config 's02' lastJpegPhotoLookup 0
php /var/www/html/occ ldap:set-config 's02' ldapCacheTTL 600
php /var/www/html/occ ldap:set-config 's02' ldapExperiencedAdmin 0  
php /var/www/html/occ ldap:set-config 's02' ldapGroupFilterMode 1  
php /var/www/html/occ ldap:set-config 's02' ldapLoginFilterEmail 0  
php /var/www/html/occ ldap:set-config 's02' ldapLoginFilterMode 0  
php /var/www/html/occ ldap:set-config 's02' ldapLoginFilterUsername 1  
php /var/www/html/occ ldap:set-config 's02' ldapNestedGroups 0  
php /var/www/html/occ ldap:set-config 's02' ldapPagingSize 500
php /var/www/html/occ ldap:set-config 's02' ldapTLS 0  
php /var/www/html/occ ldap:set-config 's02' ldapUserDisplayName displayname
php /var/www/html/occ ldap:set-config 's02' ldapUserFilterMode 0
php /var/www/html/occ ldap:set-config 's02' ldapUuidGroupAttribute auto
php /var/www/html/occ ldap:set-config 's02' ldapUuidUserAttribute auto
php /var/www/html/occ ldap:set-config 's02' turnOffCertCheck 0
php /var/www/html/occ ldap:set-config 's02' turnOnPasswordChange 0
php /var/www/html/occ ldap:set-config 's02' useMemberOfToDetectMembership 1

php /var/www/html/occ ldap:set-config 's02' ldapConfigurationActive 1
